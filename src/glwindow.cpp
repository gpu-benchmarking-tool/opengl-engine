/*__________________________________________________
 |                                                  |
 |  File: glwindow.cpp                              |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: OpenGL window.                     |
 |_________________________________________________*/



#include "glwindow.h"
#include "ui_glwindow.h"


// Constructor.
GLWindow::GLWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::GLWindow) {
    ui->setupUi(this);
    w = new GLWidget(this);
    ui->gridLayout->addWidget(w);
    this->parent = parent;
}


// Destructor.
GLWindow::~GLWindow() {
    delete ui;
    delete w;
}
