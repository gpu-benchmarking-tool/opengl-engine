/*__________________________________________________
 |                                                  |
 |    File: getgpuinfo.cpp                          |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Used to get the GPU information. |
 |_________________________________________________*/



#include "glwidget.h"


// Gets the GPU info.
void GLWidget::getGPUInfo() {

    // Get the main data
    const GLubyte* vendorByte = (glGetString(GL_VENDOR));
    const GLubyte* rendererByte = glGetString(GL_RENDERER);
    const GLubyte* versionByte = glGetString(GL_VERSION);
    const GLubyte* shadingByte = glGetString(GL_SHADING_LANGUAGE_VERSION);
    const GLubyte* extensionsByte = glGetString(GL_EXTENSIONS);

    // Convert to string
    std::string vendor(reinterpret_cast<const char*>(vendorByte));
    std::string renderer(reinterpret_cast<const char*>(rendererByte));
    std::string version(reinterpret_cast<const char*>(versionByte));
    std::string shading(reinterpret_cast<const char*>(shadingByte));
    std::string extensions(reinterpret_cast<const char*>(extensionsByte));

    // Set the main info
    profiler->setVendor(vendor);
    profiler->setRenderer(renderer);
    profiler->setGLVersion(version);
    profiler->setGLShadingLanguageVersion(shading);
    profiler->setGLExtensions(extensions);

    gpuInfoSet = true;


    // Get the additional info

    // Data returns a single value indicating the active multitexture unit. The initial value is GL_TEXTURE0. See glActiveTexture.
    glGetIntegerv(GL_ACTIVE_TEXTURE, &profiler->glInfo.gl_active_texture);

    // Data returns a pair of values indicating the range of widths supported for aliased lines. See glLineWidth.
    glGetIntegerv(GL_ALIASED_LINE_WIDTH_RANGE, &profiler->glInfo.gl_aliased_line_width_range);

    // Data returns a single value, the name of the buffer object currently bound to the target GL_ARRAY_BUFFER. If no buffer object is bound to this target, 0 is returned. The initial value is 0. See glBindBuffer.
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &profiler->glInfo.gl_array_buffer_binding);

    // Data returns a single boolean value indicating whether blending is enabled. The initial value is GL_FALSE. See glBlendFunc.
    glGetIntegerv(GL_BLEND, &profiler->glInfo.gl_blend);

    // Data returns four values, the red, green, blue, and alpha values which are the components of the blend color. See glBlendColor.
    glGetIntegerv(GL_BLEND_COLOR, &profiler->glInfo.gl_blend_color);

    // Data returns one value, the symbolic constant identifying the alpha destination blend function. The initial value is GL_ZERO. See glBlendFunc and glBlendFuncSeparate.
    glGetIntegerv(GL_BLEND_DST_ALPHA, &profiler->glInfo.gl_blend_dst_alpha);

    // Data returns one value, the symbolic constant identifying the RGB destination blend function. The initial value is GL_ZERO. See glBlendFunc and glBlendFuncSeparate.
    glGetIntegerv(GL_BLEND_DST_RGB, &profiler->glInfo.gl_blend_dst_rgb);

    // Data returns one value, a symbolic constant indicating whether the RGB blend equation is GL_FUNC_ADD, GL_FUNC_SUBTRACT, GL_FUNC_REVERSE_SUBTRACT, GL_MIN or GL_MAX. See glBlendEquationSeparate.
    glGetIntegerv(GL_BLEND_EQUATION_RGB, &profiler->glInfo.gl_blend_equation_rgb);

    // Data returns one value, a symbolic constant indicating whether the Alpha blend equation is GL_FUNC_ADD, GL_FUNC_SUBTRACT, GL_FUNC_REVERSE_SUBTRACT, GL_MIN or GL_MAX. See glBlendEquationSeparate.
    glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &profiler->glInfo.gl_blend_equation_alpha);

    // Data returns one value, the symbolic constant identifying the alpha source blend function. The initial value is GL_ONE. See glBlendFunc and glBlendFuncSeparate.
    glGetIntegerv(GL_BLEND_SRC_ALPHA, &profiler->glInfo.gl_blend_src_alpha);

    // Data returns one value, the symbolic constant identifying the RGB source blend function. The initial value is GL_ONE. See glBlendFunc and glBlendFuncSeparate.
    glGetIntegerv(GL_BLEND_SRC_RGB, &profiler->glInfo.gl_blend_src_rgb);

    // Data returns four values: the red, green, blue, and alpha values used to clear the color buffers. Integer values, if requested, are linearly mapped from the internal floating-point representation such that 1.0 returns the most positive representable integer value, and −1.0 returns the most negative representable integer value. The initial value is (0, 0, 0, 0). See glClearColor.
    glGetIntegerv(GL_COLOR_CLEAR_VALUE, &profiler->glInfo.gl_color_clear_value);

    // Data returns a single boolean value indicating whether a fragment's RGBA color values are merged into the framebuffer using a logical operation. The initial value is GL_FALSE. See glLogicOp.
    glGetIntegerv(GL_COLOR_LOGIC_OP, &profiler->glInfo.gl_color_logic_op);

    // Data returns four boolean values: the red, green, blue, and alpha write enables for the color buffers. The initial value is (GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE). See glColorMask.
    glGetIntegerv(GL_COLOR_WRITEMASK, &profiler->glInfo.gl_color_writemask);

    // Data returns a list of symbolic constants of length GL_NUM_COMPRESSED_TEXTURE_FORMATS indicating which compressed texture formats are available. See glCompressedTexImage2D.
    glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, &profiler->glInfo.gl_compressed_texture_formats);

    // Data returns one value, the maximum total number of active shader storage blocks that may be accessed by all active shaders.
    glGetIntegerv(GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS, &profiler->glInfo.gl_max_combined_shader_storage_blocks);

    // Data returns one value, the maximum number of uniform blocks per compute shader. The value must be at least 14. See glUniformBlockBinding.
    glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_BLOCKS, &profiler->glInfo.gl_max_compute_uniform_blocks);

    // Data returns one value, the maximum supported texture image units that can be used to access texture maps from the compute shader. The value may be at least 16. See glActiveTexture.
    glGetIntegerv(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS, &profiler->glInfo.gl_max_compute_texture_image_units);

    // Data returns one value, the maximum number of individual floating-point, integer, or boolean values that can be held in uniform variable storage for a compute shader. The value must be at least 1024. See glUniform.
    glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_compute_uniform_components);

    // Data returns a single value, the maximum number of atomic counters available to compute shaders.
    glGetIntegerv(GL_MAX_COMPUTE_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_compute_atomic_counters);

    // Data returns a single value, the maximum number of atomic counter buffers that may be accessed by a compute shader.
    glGetIntegerv(GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS, &profiler->glInfo.gl_max_compute_atomic_counter_buffers);

    // Data returns one value, the number of words for compute shader uniform variables in all uniform blocks (including default). The value must be at least 1. See glUniform.
    glGetIntegerv(GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_combined_compute_uniform_components);

    // Accepted by the indexed versions of glGet. data the maximum number of work groups that may be dispatched to a compute shader. Indices 0, 1, and 2 correspond to the X, Y and Z dimensions, respectively.
    glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_COUNT, &profiler->glInfo.gl_max_compute_work_group_count);

    // Accepted by the indexed versions of glGet. data the maximum size of a work groups that may be used during compilation of a compute shader. Indices 0, 1, and 2 correspond to the X, Y and Z dimensions, respectively.
    glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_SIZE, &profiler->glInfo.gl_max_compute_work_group_size);

    // Data returns a single value, the name of the buffer object currently bound to the target GL_DISPATCH_INDIRECT_BUFFER. If no buffer object is bound to this target, 0 is returned. The initial value is 0. See glBindBuffer.
    glGetIntegerv(GL_DISPATCH_INDIRECT_BUFFER_BINDING, &profiler->glInfo.gl_dispatch_indirect_buffer_binding);

    // Data returns a single value, the maximum depth of the debug message group stack.
    glGetIntegerv(GL_MAX_DEBUG_GROUP_STACK_DEPTH, &profiler->glInfo.gl_max_debug_group_stack_depth);

    // Data returns a single value, the current depth of the debug message group stack.
    glGetIntegerv(GL_DEBUG_GROUP_STACK_DEPTH, &profiler->glInfo.gl_debug_group_stack_depth);

    // Data returns one value, the flags with which the context was created (such as debugging functionality).
    glGetIntegerv(GL_CONTEXT_FLAGS, &profiler->glInfo.gl_context_flags);

    // Data returns a single boolean value indicating whether polygon culling is enabled. The initial value is GL_FALSE. See glCullFace.
    glGetIntegerv(GL_CULL_FACE, &profiler->glInfo.gl_cull_face);

    // Data returns a single value indicating the mode of polygon culling. The initial value is GL_BACK. See glCullFace.
    glGetIntegerv(GL_CULL_FACE_MODE, &profiler->glInfo.gl_cull_face_mode);

    // Data returns one value, the name of the program object that is currently active, or 0 if no program object is active. See glUseProgram.
    glGetIntegerv(GL_CURRENT_PROGRAM, &profiler->glInfo.gl_current_program);

    // Data returns one value, the value that is used to clear the depth buffer. Integer values, if requested, are linearly mapped from the internal floating-point representation such that 1.0 returns the most positive representable integer value, and −1.0 returns the most negative representable integer value. The initial value is 1. See glClearDepth.
    glGetIntegerv(GL_DEPTH_CLEAR_VALUE, &profiler->glInfo.gl_depth_clear_value);

    // Data returns one value, the symbolic constant that indicates the depth comparison function. The initial value is GL_LESS. See glDepthFunc.
    glGetIntegerv(GL_DEPTH_FUNC, &profiler->glInfo.gl_depth_func);

    // Data returns two values: the near and far mapping limits for the depth buffer. Integer values, if requested, are linearly mapped from the internal floating-point representation such that 1.0 returns the most positive representable integer value, and −1.0 returns the most negative representable integer value. The initial value is (0, 1). See glDepthRange.
    glGetIntegerv(GL_DEPTH_RANGE, &profiler->glInfo.gl_depth_range);

    // Data returns a single boolean value indicating whether depth testing of fragments is enabled. The initial value is GL_FALSE. See glDepthFunc and glDepthRange.
    glGetIntegerv(GL_DEPTH_TEST, &profiler->glInfo.gl_depth_test);

    // Data returns a single boolean value indicating if the depth buffer is enabled for writing. The initial value is GL_TRUE. See glDepthMask.
    glGetIntegerv(GL_DEPTH_WRITEMASK, &profiler->glInfo.gl_depth_writemask);

    // Data returns a single boolean value indicating whether dithering of fragment colors and indices is enabled. The initial value is GL_TRUE.
    glGetIntegerv(GL_DITHER, &profiler->glInfo.gl_dither);

    // Data returns a single boolean value indicating whether double buffering is supported.
    glGetIntegerv(GL_DOUBLEBUFFER, &profiler->glInfo.gl_doublebuffer);

    // Data returns one value, a symbolic constant indicating which buffers are being drawn to. See glDrawBuffer. The initial value is GL_BACK if there are back buffers, otherwise it is GL_FRONT.
    glGetIntegerv(GL_DRAW_BUFFER, &profiler->glInfo.gl_draw_buffer);

    // Data returns one value, the name of the framebuffer object currently bound to the GL_DRAW_FRAMEBUFFER target. If the default framebuffer is bound, this value will be zero. The initial value is zero. See glBindFramebuffer.
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &profiler->glInfo.gl_draw_framebuffer_binding);

    // Data returns one value, the name of the framebuffer object currently bound to the GL_READ_FRAMEBUFFER target. If the default framebuffer is bound, this value will be zero. The initial value is zero. See glBindFramebuffer.
    glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &profiler->glInfo.gl_read_framebuffer_binding);

    // Data returns a single value, the name of the buffer object currently bound to the target GL_ELEMENT_ARRAY_BUFFER. If no buffer object is bound to this target, 0 is returned. The initial value is 0. See glBindBuffer.
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &profiler->glInfo.gl_element_array_buffer_binding);

    // Data returns one value, a symbolic constant indicating the mode of the derivative accuracy hint for fragment shaders. The initial value is GL_DONT_CARE. See glHint.
    glGetIntegerv(GL_FRAGMENT_SHADER_DERIVATIVE_HINT, &profiler->glInfo.gl_fragment_shader_derivative_hint);

    // Data returns a single GLenum value indicating the implementation's preferred pixel data format. See glReadPixels.
    glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_FORMAT, &profiler->glInfo.gl_implementation_color_read_format);

    // Data returns a single GLenum value indicating the implementation's preferred pixel data type. See glReadPixels.
    glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_TYPE, &profiler->glInfo.gl_implementation_color_read_type);

    // Data returns a single boolean value indicating whether antialiasing of lines is enabled. The initial value is GL_FALSE. See glLineWidth.
    glGetIntegerv(GL_LINE_SMOOTH, &profiler->glInfo.gl_line_smooth);

    // Data returns one value, a symbolic constant indicating the mode of the line antialiasing hint. The initial value is GL_DONT_CARE. See glHint.
    glGetIntegerv(GL_LINE_SMOOTH_HINT, &profiler->glInfo.gl_line_smooth_hint);

    // Data returns one value, the line width as specified with glLineWidth. The initial value is 1.
    glGetIntegerv(GL_LINE_WIDTH, &profiler->glInfo.gl_line_width);

    // Data returns one value, the implementation dependent specifc vertex of a primitive that is used to select the rendering layer. If the value returned is equivalent to GL_PROVOKING_VERTEX, then the vertex selection follows the convention specified by glProvokingVertex. If the value returned is equivalent to GL_FIRST_VERTEX_CONVENTION, then the selection is always taken from the first vertex in the primitive. If the value returned is equivalent to GL_LAST_VERTEX_CONVENTION, then the selection is always taken from the last vertex in the primitive. If the value returned is equivalent to GL_UNDEFINED_VERTEX, then the selection is not guaranteed to be taken from any specific vertex in the primitive.
    glGetIntegerv(GL_LAYER_PROVOKING_VERTEX, &profiler->glInfo.gl_layer_provoking_vertex);

    // Data returns one value, a symbolic constant indicating the selected logic operation mode. The initial value is GL_COPY. See glLogicOp.
    glGetIntegerv(GL_LOGIC_OP_MODE, &profiler->glInfo.gl_logic_op_mode);

    // Data returns one value, the major version number of the OpenGL API supported by the current context.
    glGetIntegerv(GL_MAJOR_VERSION, &profiler->glInfo.gl_major_version);

    // Data returns one value, a rough estimate of the largest 3D texture that the GL can handle. The value must be at least 64. Use GL_PROXY_TEXTURE_3D to determine if a texture is too large. See glTexImage3D.
    glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &profiler->glInfo.gl_max_3d_texture_size);

    // Data returns one value. The value indicates the maximum number of layers allowed in an array texture, and must be at least 256. See glTexImage2D.
    glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &profiler->glInfo.gl_max_array_texture_layers);

    // Data returns one value, the maximum number of application-defined clipping distances. The value must be at least 8.
    glGetIntegerv(GL_MAX_CLIP_DISTANCES, &profiler->glInfo.gl_max_clip_distances);

    // Data returns one value, the maximum number of samples in a color multisample texture.
    glGetIntegerv(GL_MAX_COLOR_TEXTURE_SAMPLES, &profiler->glInfo.gl_max_color_texture_samples);

    // Data returns a single value, the maximum number of atomic counters available to all active shaders.
    glGetIntegerv(GL_MAX_COMBINED_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_combined_atomic_counters);

    // Data returns one value, the number of words for fragment shader uniform variables in all uniform blocks (including default). The value must be at least 1. See glUniform.
    glGetIntegerv(GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_combined_fragment_uniform_components);

    // Data returns one value, the number of words for geometry shader uniform variables in all uniform blocks (including default). The value must be at least 1. See glUniform.
    glGetIntegerv(GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_combined_geometry_uniform_components);

    // Data returns one value, the maximum supported texture image units that can be used to access texture maps from the vertex shader and the fragment processor combined. If both the vertex shader and the fragment processing stage access the same texture image unit, then that counts as using two texture image units against this limit. The value must be at least 48. See glActiveTexture.
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &profiler->glInfo.gl_max_combined_texture_image_units);

    // Data returns one value, the maximum number of uniform blocks per program. The value must be at least 70. See glUniformBlockBinding.
    glGetIntegerv(GL_MAX_COMBINED_UNIFORM_BLOCKS, &profiler->glInfo.gl_max_combined_uniform_blocks);

    // Data returns one value, the number of words for vertex shader uniform variables in all uniform blocks (including default). The value must be at least 1. See glUniform.
    glGetIntegerv(GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_combined_vertex_uniform_components);

    // Data returns one value. The value gives a rough estimate of the largest cube-map texture that the GL can handle. The value must be at least 1024. Use GL_PROXY_TEXTURE_CUBE_MAP to determine if a texture is too large. See glTexImage2D.
    glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, &profiler->glInfo.gl_max_cube_map_texture_size);

    // Data returns one value, the maximum number of samples in a multisample depth or depth-stencil texture.
    glGetIntegerv(GL_MAX_DEPTH_TEXTURE_SAMPLES, &profiler->glInfo.gl_max_depth_texture_samples);

    // Data returns one value, the maximum number of simultaneous outputs that may be written in a fragment shader. The value must be at least 8. See glDrawBuffers.
    glGetIntegerv(GL_MAX_DRAW_BUFFERS, &profiler->glInfo.gl_max_draw_buffers);

    // Data returns one value, the maximum number of active draw buffers when using dual-source blending. The value must be at least 1. See glBlendFunc and glBlendFuncSeparate.
    glGetIntegerv(GL_MAX_DUAL_SOURCE_DRAW_BUFFERS, &profiler->glInfo.gl_max_dual_source_draw_buffers);

    // Data returns one value, the recommended maximum number of vertex array indices. See glDrawRangeElements.
    glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &profiler->glInfo.gl_max_elements_indices);

    // Data returns one value, the recommended maximum number of vertex array vertices. See glDrawRangeElements.
    glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &profiler->glInfo.gl_max_elements_vertices);

    // Data returns a single value, the maximum number of atomic counters available to fragment shaders.
    glGetIntegerv(GL_MAX_FRAGMENT_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_fragment_atomic_counters);

    // Data returns one value, the maximum number of active shader storage blocks that may be accessed by a fragment shader.
    glGetIntegerv(GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS, &profiler->glInfo.gl_max_fragment_shader_storage_blocks);

    // Data returns one value, the maximum number of components of the inputs read by the fragment shader, which must be at least 128.
    glGetIntegerv(GL_MAX_FRAGMENT_INPUT_COMPONENTS, &profiler->glInfo.gl_max_fragment_input_components);

    // Data returns one value, the maximum number of individual floating-point, integer, or boolean values that can be held in uniform variable storage for a fragment shader. The value must be at least 1024. See glUniform.
    glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_fragment_uniform_components);

    // Data returns one value, the maximum number of individual 4-vectors of floating-point, integer, or boolean values that can be held in uniform variable storage for a fragment shader. The value is equal to the value of GL_MAX_FRAGMENT_UNIFORM_COMPONENTS divided by 4 and must be at least 256. See glUniform.
    glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &profiler->glInfo.gl_max_fragment_uniform_vectors);

    // Data returns one value, the maximum number of uniform blocks per fragment shader. The value must be at least 12. See glUniformBlockBinding.
    glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &profiler->glInfo.gl_max_fragment_uniform_blocks);

    // Data returns one value, the maximum width for a framebuffer that has no attachments, which must be at least 16384. See glFramebufferParameter.
    glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &profiler->glInfo.gl_max_framebuffer_width);

    // Data returns one value, the maximum height for a framebuffer that has no attachments, which must be at least 16384. See glFramebufferParameter.
    glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &profiler->glInfo.gl_max_framebuffer_height);

    // Data returns one value, the maximum number of layers for a framebuffer that has no attachments, which must be at least 2048. See glFramebufferParameter.
    glGetIntegerv(GL_MAX_FRAMEBUFFER_LAYERS, &profiler->glInfo.gl_max_framebuffer_layers);

    // Data returns one value, the maximum samples in a framebuffer that has no attachments, which must be at least 4. See glFramebufferParameter.
    glGetIntegerv(GL_MAX_FRAMEBUFFER_SAMPLES, &profiler->glInfo.gl_max_framebuffer_samples);

    // Data returns a single value, the maximum number of atomic counters available to geometry shaders.
    glGetIntegerv(GL_MAX_GEOMETRY_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_geometry_atomic_counters);

    // Data returns one value, the maximum number of active shader storage blocks that may be accessed by a geometry shader.
    glGetIntegerv(GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS, &profiler->glInfo.gl_max_geometry_shader_storage_blocks);

    // Data returns one value, the maximum number of components of inputs read by a geometry shader, which must be at least 64.
    glGetIntegerv(GL_MAX_GEOMETRY_INPUT_COMPONENTS, &profiler->glInfo.gl_max_geometry_input_components);

    // Data returns one value, the maximum number of components of outputs written by a geometry shader, which must be at least 128.
    glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_COMPONENTS, &profiler->glInfo.gl_max_geometry_output_components);

    // Data returns one value, the maximum supported texture image units that can be used to access texture maps from the geometry shader. The value must be at least 16. See glActiveTexture.
    glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, &profiler->glInfo.gl_max_geometry_texture_image_units);

    // Data returns one value, the maximum number of uniform blocks per geometry shader. The value must be at least 12. See glUniformBlockBinding.
    glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_BLOCKS, &profiler->glInfo.gl_max_geometry_uniform_blocks);

    // Data returns one value, the maximum number of individual floating-point, integer, or boolean values that can be held in uniform variable storage for a geometry shader. The value must be at least 1024. See glUniform.
    glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_geometry_uniform_components);

    // Data returns one value, the maximum number of samples supported in integer format multisample buffers.
    glGetIntegerv(GL_MAX_INTEGER_SAMPLES, &profiler->glInfo.gl_max_integer_samples);

    // Data returns one value, the minimum alignment in basic machine units of pointers returned fromglMapBuffer and glMapBufferRange. This value must be a power of two and must be at least 64.
    glGetIntegerv(GL_MIN_MAP_BUFFER_ALIGNMENT, &profiler->glInfo.gl_min_map_buffer_alignment);

    // Data returns one value, the maximum length of a label that may be assigned to an object. See glObjectLabel and glObjectPtrLabel.
    glGetIntegerv(GL_MAX_LABEL_LENGTH, &profiler->glInfo.gl_max_label_length);

    // Data returns one value, the maximum texel offset allowed in a texture lookup, which must be at least 7.
    glGetIntegerv(GL_MAX_PROGRAM_TEXEL_OFFSET, &profiler->glInfo.gl_max_program_texel_offset);

    // Data returns one value, the minimum texel offset allowed in a texture lookup, which must be at most -8.
    glGetIntegerv(GL_MIN_PROGRAM_TEXEL_OFFSET, &profiler->glInfo.gl_min_program_texel_offset);

    // Data returns one value. The value gives a rough estimate of the largest rectangular texture that the GL can handle. The value must be at least 1024. Use GL_PROXY_TEXTURE_RECTANGLE to determine if a texture is too large. See glTexImage2D.
    glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE, &profiler->glInfo.gl_max_rectangle_texture_size);

    // Data returns one value. The value indicates the maximum supported size for renderbuffers. See glFramebufferRenderbuffer.
    glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &profiler->glInfo.gl_max_renderbuffer_size);

    // Data returns one value, the maximum number of sample mask words.
    glGetIntegerv(GL_MAX_SAMPLE_MASK_WORDS, &profiler->glInfo.gl_max_sample_mask_words);

    // Data returns one value, the maximum glWaitSync timeout interval.
    glGetIntegerv(GL_MAX_SERVER_WAIT_TIMEOUT, &profiler->glInfo.gl_max_server_wait_timeout);

    // Data returns one value, the maximum number of shader storage buffer binding points on the context, which must be at least 8.
    glGetIntegerv(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS, &profiler->glInfo.gl_max_shader_storage_buffer_bindings);

    // Data returns a single value, the maximum number of atomic counters available to tessellation control shaders.
    glGetIntegerv(GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_tess_control_atomic_counters);

    // Data returns a single value, the maximum number of atomic counters available to tessellation evaluation shaders.
    glGetIntegerv(GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_tess_evaluation_atomic_counters);

    // Data returns one value, the maximum number of active shader storage blocks that may be accessed by a tessellation control shader.
    glGetIntegerv(GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS, &profiler->glInfo.gl_max_tess_control_shader_storage_blocks);

    // Data returns one value, the maximum number of active shader storage blocks that may be accessed by a tessellation evaluation shader.
    glGetIntegerv(GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS, &profiler->glInfo.gl_max_tess_evaluation_shader_storage_blocks);

    // Data returns one value. The value gives the maximum number of texels allowed in the texel array of a texture buffer object. Value must be at least 65536.
    glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &profiler->glInfo.gl_max_texture_buffer_size);

    // Data returns one value, the maximum supported texture image units that can be used to access texture maps from the fragment shader. The value must be at least 16. See glActiveTexture.
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &profiler->glInfo.gl_max_texture_image_units);

    // Data returns one value, the maximum, absolute value of the texture level-of-detail bias. The value must be at least 2.0.
    glGetIntegerv(GL_MAX_TEXTURE_LOD_BIAS, &profiler->glInfo.gl_max_texture_lod_bias);

    // Data returns one value. The value gives a rough estimate of the largest texture that the GL can handle. The value must be at least 1024. Use a proxy texture target such as GL_PROXY_TEXTURE_1D or GL_PROXY_TEXTURE_2D to determine if a texture is too large. See glTexImage1D and glTexImage2D.
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &profiler->glInfo.gl_max_texture_size);

    // Data returns one value, the maximum number of uniform buffer binding points on the context, which must be at least 36.
    glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &profiler->glInfo.gl_max_uniform_buffer_bindings);

    // Data returns one value, the maximum size in basic machine units of a uniform block, which must be at least 16384.
    glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &profiler->glInfo.gl_max_uniform_block_size);

    // Data returns one value, the maximum number of explicitly assignable uniform locations, which must be at least 1024.
    glGetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &profiler->glInfo.gl_max_uniform_locations);

    // Data returns one value, the number components for varying variables, which must be at least 60.
    glGetIntegerv(GL_MAX_VARYING_COMPONENTS, &profiler->glInfo.gl_max_varying_components);

    // Data returns one value, the number 4-vectors for varying variables, which is equal to the value of GL_MAX_VARYING_COMPONENTS and must be at least 15.
    glGetIntegerv(GL_MAX_VARYING_VECTORS, &profiler->glInfo.gl_max_varying_vectors);

    // Data returns one value, the maximum number of interpolators available for processing varying variables used by vertex and fragment shaders. This value represents the number of individual floating-point values that can be interpolated; varying variables declared as vectors, matrices, and arrays will all consume multiple interpolators. The value must be at least 32.
    glGetIntegerv(GL_MAX_VARYING_FLOATS, &profiler->glInfo.gl_max_varying_floats);

    // Data returns a single value, the maximum number of atomic counters available to vertex shaders.
    glGetIntegerv(GL_MAX_VERTEX_ATOMIC_COUNTERS, &profiler->glInfo.gl_max_vertex_atomic_counters);

    // Data returns one value, the maximum number of 4-component generic vertex attributes accessible to a vertex shader. The value must be at least 16. See glVertexAttrib.
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &profiler->glInfo.gl_max_vertex_attribs);

    // Data returns one value, the maximum number of active shader storage blocks that may be accessed by a vertex shader.
    glGetIntegerv(GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS, &profiler->glInfo.gl_max_vertex_shader_storage_blocks);

    // Data returns one value, the maximum supported texture image units that can be used to access texture maps from the vertex shader. The value may be at least 16. See glActiveTexture.
    glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &profiler->glInfo.gl_max_vertex_texture_image_units);

    // Data returns one value, the maximum number of individual floating-point, integer, or boolean values that can be held in uniform variable storage for a vertex shader. The value must be at least 1024. See glUniform.
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &profiler->glInfo.gl_max_vertex_uniform_components);

    // Data returns one value, the maximum number of 4-vectors that may be held in uniform variable storage for the vertex shader. The value of GL_MAX_VERTEX_UNIFORM_VECTORS is equal to the value of GL_MAX_VERTEX_UNIFORM_COMPONENTS and must be at least 256.
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &profiler->glInfo.gl_max_vertex_uniform_vectors);

    // Data returns one value, the maximum number of components of output written by a vertex shader, which must be at least 64.
    glGetIntegerv(GL_MAX_VERTEX_OUTPUT_COMPONENTS, &profiler->glInfo.gl_max_vertex_output_components);

    // Data returns one value, the maximum number of uniform blocks per vertex shader. The value must be at least 12. See glUniformBlockBinding.
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &profiler->glInfo.gl_max_vertex_uniform_blocks);

    // Data returns two values: the maximum supported width and height of the viewport. These must be at least as large as the visible dimensions of the display being rendered to. See glViewport.
    glGetIntegerv(GL_MAX_VIEWPORT_DIMS, &profiler->glInfo.gl_max_viewport_dims);

    // Data returns one value, the maximum number of simultaneous viewports that are supported. The value must be at least 16. See glViewportIndexed.
    glGetIntegerv(GL_MAX_VIEWPORTS, &profiler->glInfo.gl_max_viewports);

    // Data returns one value, the minor version number of the OpenGL API supported by the current context.
    glGetIntegerv(GL_MINOR_VERSION, &profiler->glInfo.gl_minor_version);

    // Data returns a single integer value indicating the number of available compressed texture formats. The minimum value is 4. See glCompressedTexImage2D.
    glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, &profiler->glInfo.gl_num_compressed_texture_formats);

    // Data returns one value, the number of extensions supported by the GL implementation for the current context. See glGetString.
    glGetIntegerv(GL_NUM_EXTENSIONS, &profiler->glInfo.gl_num_extensions);

    // Data returns one value, the number of program binary formats supported by the implementation.
    glGetIntegerv(GL_NUM_PROGRAM_BINARY_FORMATS, &profiler->glInfo.gl_num_program_binary_formats);

    // Data returns one value, the number of binary shader formats supported by the implementation. If this value is greater than zero, then the implementation supports loading binary shaders. If it is zero, then the loading of binary shaders by the implementation is not supported.
    glGetIntegerv(GL_NUM_SHADER_BINARY_FORMATS, &profiler->glInfo.gl_num_shader_binary_formats);

    // Data returns one value, the byte alignment used for writing pixel data to memory. The initial value is 4. See glPixelStore.
    glGetIntegerv(GL_PACK_ALIGNMENT, &profiler->glInfo.gl_pack_alignment);

    // Data returns one value, the image height used for writing pixel data to memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_PACK_IMAGE_HEIGHT, &profiler->glInfo.gl_pack_image_height);

    // Data returns a single boolean value indicating whether single-bit pixels being written to memory are written first to the least significant bit of each unsigned byte. The initial value is GL_FALSE. See glPixelStore.
    glGetIntegerv(GL_PACK_LSB_FIRST, &profiler->glInfo.gl_pack_lsb_first);

    // Data returns one value, the row length used for writing pixel data to memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_PACK_ROW_LENGTH, &profiler->glInfo.gl_pack_row_length);

    // Data returns one value, the number of pixel images skipped before the first pixel is written into memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_PACK_SKIP_IMAGES, &profiler->glInfo.gl_pack_skip_images);

    // Data returns one value, the number of pixel locations skipped before the first pixel is written into memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_PACK_SKIP_PIXELS, &profiler->glInfo.gl_pack_skip_pixels);

    // Data returns one value, the number of rows of pixel locations skipped before the first pixel is written into memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_PACK_SKIP_ROWS, &profiler->glInfo.gl_pack_skip_rows);

    // Data returns a single boolean value indicating whether the bytes of two-byte and four-byte pixel indices and components are swapped before being written to memory. The initial value is GL_FALSE. See glPixelStore.
    glGetIntegerv(GL_PACK_SWAP_BYTES, &profiler->glInfo.gl_pack_swap_bytes);

    // Data returns a single value, the name of the buffer object currently bound to the target GL_PIXEL_PACK_BUFFER. If no buffer object is bound to this target, 0 is returned. The initial value is 0. See glBindBuffer.
    glGetIntegerv(GL_PIXEL_PACK_BUFFER_BINDING, &profiler->glInfo.gl_pixel_pack_buffer_binding);

    // Data returns a single value, the name of the buffer object currently bound to the target GL_PIXEL_UNPACK_BUFFER. If no buffer object is bound to this target, 0 is returned. The initial value is 0. See glBindBuffer.
    glGetIntegerv(GL_PIXEL_UNPACK_BUFFER_BINDING, &profiler->glInfo.gl_pixel_unpack_buffer_binding);

    // Data returns one value, the point size threshold for determining the point size. See glPointParameter.
    glGetIntegerv(GL_POINT_FADE_THRESHOLD_SIZE, &profiler->glInfo.gl_point_fade_threshold_size);

    // Data returns one value, the current primitive restart index. The initial value is 0. See glPrimitiveRestartIndex.
    glGetIntegerv(GL_PRIMITIVE_RESTART_INDEX, &profiler->glInfo.gl_primitive_restart_index);

    // Data an array of GL_NUM_PROGRAM_BINARY_FORMATS values, indicating the proram binary formats supported by the implementation.
    glGetIntegerv(GL_PROGRAM_BINARY_FORMATS, &profiler->glInfo.gl_program_binary_formats);

    // Data a single value, the name of the currently bound program pipeline object, or zero if no program pipeline object is bound. See glBindProgramPipeline.
    glGetIntegerv(GL_PROGRAM_PIPELINE_BINDING, &profiler->glInfo.gl_program_pipeline_binding);

    // Data returns a single boolean value indicating whether vertex program point size mode is enabled. If enabled, then the point size is taken from the shader built-in gl_PointSize. If disabled, then the point size is taken from the point state as specified by glPointSize. The initial value is GL_FALSE.
    glGetIntegerv(GL_PROGRAM_POINT_SIZE, &profiler->glInfo.gl_program_point_size);

    // Data returns one value, the currently selected provoking vertex convention. The initial value is GL_LAST_VERTEX_CONVENTION. See glProvokingVertex.
    glGetIntegerv(GL_PROVOKING_VERTEX, &profiler->glInfo.gl_provoking_vertex);

    // Data returns one value, the point size as specified by glPointSize. The initial value is 1.
    glGetIntegerv(GL_POINT_SIZE, &profiler->glInfo.gl_point_size);

    // Data returns one value, the size difference between adjacent supported sizes for antialiased points. See glPointSize.
    glGetIntegerv(GL_POINT_SIZE_GRANULARITY, &profiler->glInfo.gl_point_size_granularity);

    // Data returns two values: the smallest and largest supported sizes for antialiased points. The smallest size must be at most 1, and the largest size must be at least 1. See glPointSize.
    glGetIntegerv(GL_POINT_SIZE_RANGE, &profiler->glInfo.gl_point_size_range);

    // Data returns one value, the scaling factor used to determine the variable offset that is added to the depth value of each fragment generated when a polygon is rasterized. The initial value is 0. See glPolygonOffset.
    glGetIntegerv(GL_POLYGON_OFFSET_FACTOR, &profiler->glInfo.gl_polygon_offset_factor);

    // Data returns one value. This value is multiplied by an implementation-specific value and then added to the depth value of each fragment generated when a polygon is rasterized. The initial value is 0. See glPolygonOffset.
    glGetIntegerv(GL_POLYGON_OFFSET_UNITS, &profiler->glInfo.gl_polygon_offset_units);

    // Data returns a single boolean value indicating whether polygon offset is enabled for polygons in fill mode. The initial value is GL_FALSE. See glPolygonOffset.
    glGetIntegerv(GL_POLYGON_OFFSET_FILL, &profiler->glInfo.gl_polygon_offset_fill);

    // Data returns a single boolean value indicating whether polygon offset is enabled for polygons in line mode. The initial value is GL_FALSE. See glPolygonOffset.
    glGetIntegerv(GL_POLYGON_OFFSET_LINE, &profiler->glInfo.gl_polygon_offset_line);

    // Data returns a single boolean value indicating whether polygon offset is enabled for polygons in point mode. The initial value is GL_FALSE. See glPolygonOffset.
    glGetIntegerv(GL_POLYGON_OFFSET_POINT, &profiler->glInfo.gl_polygon_offset_point);

    // Data returns a single boolean value indicating whether antialiasing of polygons is enabled. The initial value is GL_FALSE. See glPolygonMode.
    glGetIntegerv(GL_POLYGON_SMOOTH, &profiler->glInfo.gl_polygon_smooth);

    // Data returns one value, a symbolic constant indicating the mode of the polygon antialiasing hint. The initial value is GL_DONT_CARE. See glHint.
    glGetIntegerv(GL_POLYGON_SMOOTH_HINT, &profiler->glInfo.gl_polygon_smooth_hint);

    // Data returns one value, a symbolic constant indicating which color buffer is selected for reading. The initial value is GL_BACK if there is a back buffer, otherwise it is GL_FRONT. See glReadPixels.
    glGetIntegerv(GL_READ_BUFFER, &profiler->glInfo.gl_read_buffer);

    // Data returns a single value, the name of the renderbuffer object currently bound to the target GL_RENDERBUFFER. If no renderbuffer object is bound to this target, 0 is returned. The initial value is 0. See glBindRenderbuffer.
    glGetIntegerv(GL_RENDERBUFFER_BINDING, &profiler->glInfo.gl_renderbuffer_binding);

    // Data returns a single integer value indicating the number of sample buffers associated with the framebuffer. See glSampleCoverage.
    glGetIntegerv(GL_SAMPLE_BUFFERS, &profiler->glInfo.gl_sample_buffers);

    // Data returns a single positive floating-point value indicating the current sample coverage value. See glSampleCoverage.
    glGetIntegerv(GL_SAMPLE_COVERAGE_VALUE, &profiler->glInfo.gl_sample_coverage_value);

    // Data returns a single boolean value indicating if the temporary coverage value should be inverted. See glSampleCoverage.
    glGetIntegerv(GL_SAMPLE_COVERAGE_INVERT, &profiler->glInfo.gl_sample_coverage_invert);

    // Data returns a single value, the name of the sampler object currently bound to the active texture unit. The initial value is 0. See glBindSampler.
    glGetIntegerv(GL_SAMPLER_BINDING, &profiler->glInfo.gl_sampler_binding);

    // Data returns a single integer value indicating the coverage mask size. See glSampleCoverage.
    glGetIntegerv(GL_SAMPLES, &profiler->glInfo.gl_samples);

    // Data returns four values: the x and y window coordinates of the scissor box, followed by its width and height. Initially the x and y window coordinates are both 0 and the width and height are set to the size of the window. See glScissor.
    glGetIntegerv(GL_SCISSOR_BOX, &profiler->glInfo.gl_scissor_box);

    // Data returns a single boolean value indicating whether scissoring is enabled. The initial value is GL_FALSE. See glScissor.
    glGetIntegerv(GL_SCISSOR_TEST, &profiler->glInfo.gl_scissor_test);

    // Data returns a single boolean value indicating whether an online shader compiler is present in the implementation. All desktop OpenGL implementations must support online shader compilations, and therefore the value of GL_SHADER_COMPILER will always be GL_TRUE.
    glGetIntegerv(GL_SHADER_COMPILER, &profiler->glInfo.gl_shader_compiler);

    // When used with non-indexed variants of glGet (such as glGetIntegerv), data returns a single value, the name of the buffer object currently bound to the target GL_SHADER_STORAGE_BUFFER. If no buffer object is bound to this target, 0 is returned. When used with indexed variants of glGet (such as glGetIntegeri_v), data returns a single value, the name of the buffer object bound to the indexed shader storage buffer binding points. The initial value is 0 for all targets. See glBindBuffer, glBindBufferBase, and glBindBufferRange.
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_BINDING, &profiler->glInfo.gl_shader_storage_buffer_binding);

    // Data returns a single value, the minimum required alignment for shader storage buffer sizes and offset. The initial value is 1. See glShaderStorageBlockBinding.
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT, &profiler->glInfo.gl_shader_storage_buffer_offset_alignment);

    // When used with indexed variants of glGet (such as glGetInteger64i_v), data returns a single value, the start offset of the binding range for each indexed shader storage buffer binding. The initial value is 0 for all bindings. See glBindBufferRange.
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_START, &profiler->glInfo.gl_shader_storage_buffer_start);

    // When used with indexed variants of glGet (such as glGetInteger64i_v), data returns a single value, the size of the binding range for each indexed shader storage buffer binding. The initial value is 0 for all bindings. See glBindBufferRange.
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_SIZE, &profiler->glInfo.gl_shader_storage_buffer_size);

    // Data returns a pair of values indicating the range of widths supported for smooth (antialiased) lines. See glLineWidth.
    glGetIntegerv(GL_SMOOTH_LINE_WIDTH_RANGE, &profiler->glInfo.gl_smooth_line_width_range);

    // Data returns a single value indicating the level of quantization applied to smooth line width parameters.
    glGetIntegerv(GL_SMOOTH_LINE_WIDTH_GRANULARITY, &profiler->glInfo.gl_smooth_line_width_granularity);

    // Data returns one value, a symbolic constant indicating what action is taken for back-facing polygons when the stencil test fails. The initial value is GL_KEEP. See glStencilOpSeparate.
    glGetIntegerv(GL_STENCIL_BACK_FAIL, &profiler->glInfo.gl_stencil_back_fail);

    // Data returns one value, a symbolic constant indicating what function is used for back-facing polygons to compare the stencil reference value with the stencil buffer value. The initial value is GL_ALWAYS. See glStencilFuncSeparate.
    glGetIntegerv(GL_STENCIL_BACK_FUNC, &profiler->glInfo.gl_stencil_back_func);

    // Data returns one value, a symbolic constant indicating what action is taken for back-facing polygons when the stencil test passes, but the depth test fails. The initial value is GL_KEEP. See glStencilOpSeparate.
    glGetIntegerv(GL_STENCIL_BACK_PASS_DEPTH_FAIL, &profiler->glInfo.gl_stencil_back_pass_depth_fail);

    // Data returns one value, a symbolic constant indicating what action is taken for back-facing polygons when the stencil test passes and the depth test passes. The initial value is GL_KEEP. See glStencilOpSeparate.
    glGetIntegerv(GL_STENCIL_BACK_PASS_DEPTH_PASS, &profiler->glInfo.gl_stencil_back_pass_depth_pass);

    // Data returns one value, the reference value that is compared with the contents of the stencil buffer for back-facing polygons. The initial value is 0. See glStencilFuncSeparate.
    glGetIntegerv(GL_STENCIL_BACK_REF, &profiler->glInfo.gl_stencil_back_ref);

    // Data returns one value, the mask that is used for back-facing polygons to mask both the stencil reference value and the stencil buffer value before they are compared. The initial value is all 1's. See glStencilFuncSeparate.
    glGetIntegerv(GL_STENCIL_BACK_VALUE_MASK, &profiler->glInfo.gl_stencil_back_value_mask);

    // Data returns one value, the mask that controls writing of the stencil bitplanes for back-facing polygons. The initial value is all 1's. See glStencilMaskSeparate.
    glGetIntegerv(GL_STENCIL_BACK_WRITEMASK, &profiler->glInfo.gl_stencil_back_writemask);

    // Data returns one value, the index to which the stencil bitplanes are cleared. The initial value is 0. See glClearStencil.
    glGetIntegerv(GL_STENCIL_CLEAR_VALUE, &profiler->glInfo.gl_stencil_clear_value);

    // Data returns one value, a symbolic constant indicating what action is taken when the stencil test fails. The initial value is GL_KEEP. See glStencilOp. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilOpSeparate.
    glGetIntegerv(GL_STENCIL_FAIL, &profiler->glInfo.gl_stencil_fail);

    // Data returns one value, a symbolic constant indicating what function is used to compare the stencil reference value with the stencil buffer value. The initial value is GL_ALWAYS. See glStencilFunc. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilFuncSeparate.
    glGetIntegerv(GL_STENCIL_FUNC, &profiler->glInfo.gl_stencil_func);

    // Data returns one value, a symbolic constant indicating what action is taken when the stencil test passes, but the depth test fails. The initial value is GL_KEEP. See glStencilOp. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilOpSeparate.
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_FAIL, &profiler->glInfo.gl_stencil_pass_depth_fail);

    // Data returns one value, a symbolic constant indicating what action is taken when the stencil test passes and the depth test passes. The initial value is GL_KEEP. See glStencilOp. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilOpSeparate.
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_PASS, &profiler->glInfo.gl_stencil_pass_depth_pass);

    // Data returns one value, the reference value that is compared with the contents of the stencil buffer. The initial value is 0. See glStencilFunc. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilFuncSeparate.
    glGetIntegerv(GL_STENCIL_REF, &profiler->glInfo.gl_stencil_ref);

    // Data returns a single boolean value indicating whether stencil testing of fragments is enabled. The initial value is GL_FALSE. See glStencilFunc and glStencilOp.
    glGetIntegerv(GL_STENCIL_TEST, &profiler->glInfo.gl_stencil_test);

    // Data returns one value, the mask that is used to mask both the stencil reference value and the stencil buffer value before they are compared. The initial value is all 1's. See glStencilFunc. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilFuncSeparate.
    glGetIntegerv(GL_STENCIL_VALUE_MASK, &profiler->glInfo.gl_stencil_value_mask);

    // Data returns one value, the mask that controls writing of the stencil bitplanes. The initial value is all 1's. See glStencilMask. This stencil state only affects non-polygons and front-facing polygons. Back-facing polygons use separate stencil state. See glStencilMaskSeparate.
    glGetIntegerv(GL_STENCIL_WRITEMASK, &profiler->glInfo.gl_stencil_writemask);

    // Data returns a single boolean value indicating whether stereo buffers (left and right) are supported.
    glGetIntegerv(GL_STEREO, &profiler->glInfo.gl_stereo);

    // Data returns one value, an estimate of the number of bits of subpixel resolution that are used to position rasterized geometry in window coordinates. The value must be at least 4.
    glGetIntegerv(GL_SUBPIXEL_BITS, &profiler->glInfo.gl_subpixel_bits);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_1D. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_1D, &profiler->glInfo.gl_texture_binding_1d);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_1D_ARRAY. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_1D_ARRAY, &profiler->glInfo.gl_texture_binding_1d_array);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_2D. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &profiler->glInfo.gl_texture_binding_2d);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_2D_ARRAY. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_2D_ARRAY, &profiler->glInfo.gl_texture_binding_2d_array);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_2D_MULTISAMPLE. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_2D_MULTISAMPLE, &profiler->glInfo.gl_texture_binding_2d_multisample);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_2D_MULTISAMPLE_ARRAY. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY, &profiler->glInfo.gl_texture_binding_2d_multisample_array);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_3D. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_3D, &profiler->glInfo.gl_texture_binding_3d);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_CUBE_MAP. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_CUBE_MAP, &profiler->glInfo.gl_texture_binding_cube_map);

    // Data returns a single value, the name of the texture currently bound to the target GL_TEXTURE_RECTANGLE. The initial value is 0. See glBindTexture.
    glGetIntegerv(GL_TEXTURE_BINDING_RECTANGLE, &profiler->glInfo.gl_texture_binding_rectangle);

    // Data returns a single value indicating the mode of the texture compression hint. The initial value is GL_DONT_CARE.
    glGetIntegerv(GL_TEXTURE_COMPRESSION_HINT, &profiler->glInfo.gl_texture_compression_hint);

    // Data returns a single value, the name of the buffer object currently bound to the GL_TEXTURE_BUFFER buffer binding point. The initial value is 0. See glBindBuffer.
    glGetIntegerv(GL_TEXTURE_BINDING_BUFFER, &profiler->glInfo.gl_texture_binding_buffer);

    // Data returns a single value, the minimum required alignment for texture buffer sizes and offset. The initial value is 1. See glUniformBlockBinding.
    glGetIntegerv(GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT, &profiler->glInfo.gl_texture_buffer_offset_alignment);

    // Data returns a single value, the 64-bit value of the current GL time. See glQueryCounter.
    glGetIntegerv(GL_TIMESTAMP, &profiler->glInfo.gl_timestamp);

    // When used with non-indexed variants of glGet (such as glGetIntegerv), data returns a single value, the name of the buffer object currently bound to the target GL_TRANSFORM_FEEDBACK_BUFFER. If no buffer object is bound to this target, 0 is returned. When used with indexed variants of glGet (such as glGetIntegeri_v), data returns a single value, the name of the buffer object bound to the indexed transform feedback attribute stream. The initial value is 0 for all targets. See glBindBuffer, glBindBufferBase, and glBindBufferRange.
    glGetIntegerv(GL_TRANSFORM_FEEDBACK_BUFFER_BINDING, &profiler->glInfo.gl_transform_feedback_buffer_binding);

    // When used with indexed variants of glGet (such as glGetInteger64i_v), data returns a single value, the start offset of the binding range for each transform feedback attribute stream. The initial value is 0 for all streams. See glBindBufferRange.
    glGetIntegerv(GL_TRANSFORM_FEEDBACK_BUFFER_START, &profiler->glInfo.gl_transform_feedback_buffer_start);

    // When used with indexed variants of glGet (such as glGetInteger64i_v), data returns a single value, the size of the binding range for each transform feedback attribute stream. The initial value is 0 for all streams. See glBindBufferRange.
    glGetIntegerv(GL_TRANSFORM_FEEDBACK_BUFFER_SIZE, &profiler->glInfo.gl_transform_feedback_buffer_size);

    // When used with non-indexed variants of glGet (such as glGetIntegerv), data returns a single value, the name of the buffer object currently bound to the target GL_UNIFORM_BUFFER. If no buffer object is bound to this target, 0 is returned. When used with indexed variants of glGet (such as glGetIntegeri_v), data returns a single value, the name of the buffer object bound to the indexed uniform buffer binding point. The initial value is 0 for all targets. See glBindBuffer, glBindBufferBase, and glBindBufferRange.
    glGetIntegerv(GL_UNIFORM_BUFFER_BINDING, &profiler->glInfo.gl_uniform_buffer_binding);

    // Data returns a single value, the minimum required alignment for uniform buffer sizes and offset. The initial value is 1. See glUniformBlockBinding.
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &profiler->glInfo.gl_uniform_buffer_offset_alignment);

    // When used with indexed variants of glGet (such as glGetInteger64i_v), data returns a single value, the size of the binding range for each indexed uniform buffer binding. The initial value is 0 for all bindings. See glBindBufferRange.
    glGetIntegerv(GL_UNIFORM_BUFFER_SIZE, &profiler->glInfo.gl_uniform_buffer_size);

    // When used with indexed variants of glGet (such as glGetInteger64i_v), data returns a single value, the start offset of the binding range for each indexed uniform buffer binding. The initial value is 0 for all bindings. See glBindBufferRange.
    glGetIntegerv(GL_UNIFORM_BUFFER_START, &profiler->glInfo.gl_uniform_buffer_start);

    // Data returns one value, the byte alignment used for reading pixel data from memory. The initial value is 4. See glPixelStore.
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &profiler->glInfo.gl_unpack_alignment);

    // Data returns one value, the image height used for reading pixel data from memory. The initial is 0. See glPixelStore.
    glGetIntegerv(GL_UNPACK_IMAGE_HEIGHT, &profiler->glInfo.gl_unpack_image_height);

    // Data returns a single boolean value indicating whether single-bit pixels being read from memory are read first from the least significant bit of each unsigned byte. The initial value is GL_FALSE. See glPixelStore.
    glGetIntegerv(GL_UNPACK_LSB_FIRST, &profiler->glInfo.gl_unpack_lsb_first);

    // Data returns one value, the row length used for reading pixel data from memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_UNPACK_ROW_LENGTH, &profiler->glInfo.gl_unpack_row_length);

    // Data returns one value, the number of pixel images skipped before the first pixel is read from memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_UNPACK_SKIP_IMAGES, &profiler->glInfo.gl_unpack_skip_images);

    // Data returns one value, the number of pixel locations skipped before the first pixel is read from memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_UNPACK_SKIP_PIXELS, &profiler->glInfo.gl_unpack_skip_pixels);

    // Data returns one value, the number of rows of pixel locations skipped before the first pixel is read from memory. The initial value is 0. See glPixelStore.
    glGetIntegerv(GL_UNPACK_SKIP_ROWS, &profiler->glInfo.gl_unpack_skip_rows);

    // Data returns a single boolean value indicating whether the bytes of two-byte and four-byte pixel indices and components are swapped after being read from memory. The initial value is GL_FALSE. See glPixelStore.
    glGetIntegerv(GL_UNPACK_SWAP_BYTES, &profiler->glInfo.gl_unpack_swap_bytes);

    // Data returns a single value, the name of the vertex array object currently bound to the context. If no vertex array object is bound to the context, 0 is returned. The initial value is 0. See glBindVertexArray.
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &profiler->glInfo.gl_vertex_array_binding);

    // Accepted by the indexed forms. data returns a single integer value representing the instance step divisor of the first element in the bound buffer's data store for vertex attribute bound to index.
    glGetIntegerv(GL_VERTEX_BINDING_DIVISOR, &profiler->glInfo.gl_vertex_binding_divisor);

    // Accepted by the indexed forms. data returns a single integer value representing the byte offset of the first element in the bound buffer's data store for vertex attribute bound to index.
    glGetIntegerv(GL_VERTEX_BINDING_OFFSET, &profiler->glInfo.gl_vertex_binding_offset);

    // Accepted by the indexed forms. data returns a single integer value representing the byte offset between the start of each element in the bound buffer's data store for vertex attribute bound to index.
    glGetIntegerv(GL_VERTEX_BINDING_STRIDE, &profiler->glInfo.gl_vertex_binding_stride);

    // Data returns a single integer value containing the maximum offset that may be added to a vertex binding offset.
    glGetIntegerv(GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET, &profiler->glInfo.gl_max_vertex_attrib_relative_offset);

    // Data returns a single integer value containing the maximum number of vertex buffers that may be bound.
    glGetIntegerv(GL_MAX_VERTEX_ATTRIB_BINDINGS, &profiler->glInfo.gl_max_vertex_attrib_bindings);

    // When used with non-indexed variants of glGet (such as glGetIntegerv), data returns four values: the x and y window coordinates of the viewport, followed by its width and height. Initially the x and y window coordinates are both set to 0, and the width and height are set to the width and height of the window into which the GL will do its rendering. See glViewport.
    glGetIntegerv(GL_VIEWPORT, &profiler->glInfo.gl_viewport);

    // Data returns two values, the minimum and maximum viewport bounds range. The minimum range should be at least [-32768, 32767].
    glGetIntegerv(GL_VIEWPORT_BOUNDS_RANGE, &profiler->glInfo.gl_viewport_bounds_range);

    // Data returns one value, the implementation dependent specifc vertex of a primitive that is used to select the viewport index. If the value returned is equivalent to GL_PROVOKING_VERTEX, then the vertex selection follows the convention specified by glProvokingVertex. If the value returned is equivalent to GL_FIRST_VERTEX_CONVENTION, then the selection is always taken from the first vertex in the primitive. If the value returned is equivalent to GL_LAST_VERTEX_CONVENTION, then the selection is always taken from the last vertex in the primitive. If the value returned is equivalent to GL_UNDEFINED_VERTEX, then the selection is not guaranteed to be taken from any specific vertex in the primitive.
    glGetIntegerv(GL_VIEWPORT_INDEX_PROVOKING_VERTEX, &profiler->glInfo.gl_viewport_index_provoking_vertex);

    // Data returns a single value, the number of bits of sub-pixel precision which the GL uses to interpret the floating point viewport bounds. The minimum value is 0.
    glGetIntegerv(GL_VIEWPORT_SUBPIXEL_BITS, &profiler->glInfo.gl_viewport_subpixel_bits);

    // Data returns a single value, the maximum index that may be specified during the transfer of generic vertex attributes to the GL.
    glGetIntegerv(GL_MAX_ELEMENT_INDEX, &profiler->glInfo.gl_max_element_index);
}
