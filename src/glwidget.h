/*__________________________________________________
 |                                                  |
 |  File: glwidget.h                                |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: OpenGL widget class.               |
 |_________________________________________________*/



#pragma once

#include <QtGlobal>
#if QT_VERSION < QT_VERSION_CHECK(5, 4, 0)
#include <QGLWidget>
#else
#include <QOpenGLWidget>
#endif

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QKeyEvent>
#include <QDebug>
#include <QElapsedTimer>
#include <QTimer>

#include <map>

#include "model.hpp"
#include "libs/Profiler.h"


/**
 * @brief      Class for gl widget.
 * OpenGL Widget class based on QGLWidget and QOpenGLFunctions to implement the OpenGL API.
 */
#if QT_VERSION < QT_VERSION_CHECK(5, 4, 0)
class GLWidget : public QGLWidget, protected QOpenGLFunctions
#else
class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
#endif
{
    Q_OBJECT

  private:
    QMatrix4x4 projection;
    QTimer timer;
    std::vector<Model> models;
    gpu::Profiler *profiler = nullptr;
    short modelIndex;
    short numberOfLevels;
    int gridSize;
    float angle;
    QWidget *parent;
    bool gpuInfoSet;
    int targetFramerate;


  public:

    /**
    * @brief      Constructs the object.
     * Builds the OpenGL widget.
    *
    * @param      parent  The parent window
    */
    GLWidget(QWidget *parent = nullptr);


    /**
     * @brief      Destroys the object.
     */
    ~GLWidget() {}


    /**
     * @brief      Sets the scene.
     *
     * @return     True if the scene is loaded successfully, False otherwise.
     */
    bool setScene();


    /**
     * @brief      Sets the profiler.
     * Profiler passed as a pointer so it can be retrieved by the parent.
     *
     * @param      p     GPU Profiler
     */
    void setProfiler(gpu::Profiler *p) { this->profiler = p; }


    /**
     * @brief      Sets target refresh rate.
     *
     * @param      framerate     Target framerate
     */
    void setTargetFramerate(int framerate) { this->targetFramerate = framerate; }


  protected:

    /**
     * @brief      Initialises OpenGL context.
     */
    void initializeGL();


    /**
     * @brief      Rendering function.
     * Binds the model data and renders each cube of the scene.
     */
    void paintGL();


    /**
     * @brief      Handles widget resize based on the window size.
     *
     * @param[in]  width   The window width
     * @param[in]  height  The window height
     */
    void resizeGL(int width, int height);


  private slots:

    /**
     * @brief      Update function.
     * Prepares the next frame before rendering it.
     */
    void update();


    /**
     * @brief      Gets the gpu information.
     */
    void getGPUInfo();
};
