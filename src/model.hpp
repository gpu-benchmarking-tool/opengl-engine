/*__________________________________________________
 |                                                  |
 |  File: model.hpp                                 |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Model class.                       |
 |_________________________________________________*/



#ifndef MODEL_HPP
#define MODEL_HPP

#include "libs/OBJ.h"
#include <vector>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>


/**
 * @brief      Class for model.
 * 3D model loaded as an OBJ.
 * Has a reference to the OBJ instance, the buffers (vertex and UVs) buffers.
 * Contains model array object, shader program and texture.
 */
class Model {

  public:
    obj::OBJ obj;
    std::vector<unsigned int> indices;
    QOpenGLBuffer vertexBuffer, uvsBuffer;
    QOpenGLVertexArrayObject modelObject;
    QOpenGLShaderProgram *shaderProgram;
    QOpenGLTexture *texture;


    /**
     * @brief      Constructs the object.
     * Empty constructor.
     */
    Model() {}


    /**
     * @brief      Constructs the object.
     * Copy constructor.
     *
     * @param[in]  m     Model to copy
     */
    Model(const Model& m) { Q_UNUSED(m) }


    /**
     * @brief      Destroys the object.
     * Deletes the shader program and the texture.
     */
    ~Model() {
        delete shaderProgram;
        delete texture;
    }


    /**
     * @brief      Setups the model.
     * Creates the shader model, creates and binds the buffers and loads the shaders.
     */
    void setup() {

        // Create program
        shaderProgram = new QOpenGLShaderProgram();

        // Load shaders
        shaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/assets/Shaders/opengl.vert");
        shaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/assets/Shaders/opengl.frag");

        shaderProgram->link();
        shaderProgram->bind();

        // Create object
        modelObject.create();
        modelObject.bind();

        // Create vertex buffer
        vertexBuffer.create();
        vertexBuffer.bind();
        vertexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        vertexBuffer.allocate(obj.getVertices().data(), static_cast<int>(obj.getVertices().size() * sizeof(math::Vector3)));

        // Pass vertex position to shaders
        shaderProgram->enableAttributeArray("position");
        shaderProgram->setAttributeBuffer("position", GL_DOUBLE, 0, 3, sizeof(math::Vector3));
        vertexBuffer.release();

        // Create texture coordinates buffer
        uvsBuffer.create();
        uvsBuffer.bind();
        uvsBuffer.allocate(obj.getUVs().data(), static_cast<int>(obj.getUVs().size() * sizeof(math::Vector2)));

        // Pass texture coordinates to shaders
        shaderProgram->enableAttributeArray("textureCoordinate");
        shaderProgram->setAttributeBuffer("textureCoordinate", GL_DOUBLE, 0, 2, sizeof(math::Vector2));
        uvsBuffer.release();

        // Load texture
        texture = new QOpenGLTexture(QImage(":/assets/OBJ/texture.png").mirrored());

        // Release
        modelObject.release();
        shaderProgram->release();
    }
};

#endif
