/*__________________________________________________
 |                                                  |
 |  File: glwidget.cpp                              |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: OpenGL widget class.               |
 |_________________________________________________*/



#include "glwidget.h"
#include <QFile>
#include <QDir>
#include <omp.h>
#define QT_APPLICATION
#include "libs/Triangulate.hpp"


// Constructor.
#if QT_VERSION < QT_VERSION_CHECK(5, 4, 0)
GLWidget::GLWidget(QWidget * parent) : QGLWidget(parent)
#else
GLWidget::GLWidget(QWidget * parent) : QOpenGLWidget(parent)
#endif
{

    // Initialise the values
    modelIndex = 0;
    numberOfLevels = 7;
    gridSize = 1;
    gpuInfoSet = false;
    targetFramerate = 60;
    angle = 0;

    // Set the scene
    if (!setScene()) qFatal("Failed to load cube.obj");

    // Start
    connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
    this->parent = parent;
}


// Sets the scene.
bool GLWidget::setScene() {

    // Set the number of models
    models.resize(static_cast<unsigned long>(numberOfLevels));

    // Load the models
    for (short i = 0; i < numberOfLevels; ++i) {
        models[static_cast<unsigned long>(i)].obj.loadModel(("./assets/cube" + QString::number(i) + ".obj").toUtf8());
        if (!models[static_cast<unsigned long>(i)].obj.isLoaded())
            return false;
    }

    // Build the indices lists
    #pragma omp parallel for

    for (unsigned long i = 0; i < models.size(); ++i) {
        std::vector<obj::Face> faces = models[i].obj.getFaces();

        for (unsigned long j = 0; j < faces.size(); ++j) {

            models[i].indices.push_back(faces[j].v0);
            models[i].indices.push_back(faces[j].v1);
            models[i].indices.push_back(faces[j].v2);
        }
    }

    return true;
}


// Initialises openGL and loads shaders/buffers.
void GLWidget::initializeGL() {

    // Initialises opengl functions
    initializeOpenGLFunctions();

    // Set clear colour
    glClearColor(0, 0, 0, 1);

    // Enablers
    glEnable(GL_DEPTH_TEST);

    // Setup the models
    for (unsigned long i = 0; i < models.size(); ++i)
        models[i].setup();
}


// Resizes the openGL widget.
void GLWidget::resizeGL(int width, int height) {

    Q_UNUSED(width);
    Q_UNUSED(height);

#if QT_VERSION < QT_VERSION_CHECK(5, 4, 0)
    glViewport(0, 0, width, height);
#else
    // Reset projection
    projection.setToIdentity();
    projection.ortho(-1, 1, -1, 1, 0, 15);
#endif
}


// Draw function.
void GLWidget::paintGL() {

    // Get the GPU info
    if (!gpuInfoSet && profiler) getGPUInfo();

    // Start the timer
    QElapsedTimer chrono;
    chrono.start();

    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Bind
    models[static_cast<unsigned long>(modelIndex)].shaderProgram->bind();
    models[static_cast<unsigned long>(modelIndex)].modelObject.bind();
    models[static_cast<unsigned long>(modelIndex)].texture->bind();
    models[static_cast<unsigned long>(modelIndex)].shaderProgram->setUniformValue("a_texture", 0);

    // Draw the grid of cubes
    for (short i = 0; i < gridSize; ++i) {
        for (short j = 0; j < gridSize; ++j) {

            // Compute the starting position
            int startPos = static_cast<int>(-0.99 * gridSize);

            // Compute the scale
            float scale = 1 / static_cast<float>(gridSize);

            // Update the mvp matrix
            QMatrix4x4 matrix;
            matrix.scale(scale, scale, scale);
            matrix.translate(startPos + static_cast<int>(i * 2), startPos + static_cast<int>(j * 2), -2.0);
            matrix.rotate(QQuaternion::fromAxisAndAngle(0, 1, 0, angle));
            models[modelIndex].shaderProgram->setUniformValue("mvp_matrix", projection * matrix);

            // Draw the elements
            glDrawElements(GL_TRIANGLES, models[modelIndex].indices.size(), GL_UNSIGNED_INT, &models[modelIndex].indices[0]);
        }
    }

    // Release
    models[static_cast<unsigned long>(modelIndex)].modelObject.release();
    models[static_cast<unsigned long>(modelIndex)].shaderProgram->release();

    // Record elpased time
    qint64 elapsed = chrono.elapsed();

    // Add entry to profiler
    if (profiler) profiler->addTrianglesEntry(models[static_cast<unsigned long>(modelIndex)].obj.getFaces().size() * gridSize * gridSize, elapsed);

    // If it takes more than 16ms (60 FPS) we stop
    if (elapsed > 1000. / targetFramerate) parent->close();
}


// Update function.
void GLWidget::update() {

    // Increment angle
    angle = angle >= 360 ? 1 : angle + 1;

    // Increment model index / grid size
    if (modelIndex == numberOfLevels - 1) gridSize++;
    else modelIndex++;

    repaint();
}


