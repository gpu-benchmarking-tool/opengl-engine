/*__________________________________________________
 |                                                  |
 |  File: main.cpp                                  |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Main function.                     |
 |_________________________________________________*/



#include "glwindow.h"
#include <QApplication>


// Main function.
int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    a.setAttribute(Qt::AA_UseDesktopOpenGL);
    GLWindow w;
    w.show();

    return a.exec();
}
