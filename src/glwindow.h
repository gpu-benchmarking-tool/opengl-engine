/*__________________________________________________
 |                                                  |
 |  File: glwindow.h                                |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: OpenGL window.                     |
 |_________________________________________________*/



#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <QMainWindow>
#include "glwidget.h"


namespace Ui {
class GLWindow;
}


/**
 * @brief      Class for gl window.
 * OpenGL window based on QMainWindow widget.
 */
class GLWindow : public QMainWindow {

    Q_OBJECT

  public:

    /**
     * @brief      Constructs the object.
     * Builds the GL Window. Keeps a refernce to the parent window.
     *
     * @param      parent  The parent window
     */
    explicit GLWindow(QWidget *parent = nullptr);


    /**
     * @brief      Destroys the object.
     */
    ~GLWindow();


    /**
     * @brief      Sets the profiler.
     * Profiler passed as a pointer so that the data can be retrieved by the parent.
     *
     * @param      p     Profiler used to record the data
     */
    void setProfiler(gpu::Profiler *p) { w->setProfiler(p); }


    /**
     * @brief      Sets target framerate.
     * Assigns target refresh rate.
     *
     * @param      framerate     Target framerate
     */
    void setTargetFramerate(int framerate) { w->setTargetFramerate(framerate); }


  private:
    Ui::GLWindow *ui;
    GLWidget *w;
    QWidget *parent;
};

#endif
