cd src
rmdir /s /Q libs
cd ..

git submodule update --init --recursive
git submodule update --recursive --remote

cd libs\obj-loader
call build.bat
cd ..\..

cd profiler
call build.bat
cd ..\..\..

cd src
mkdir libs

copy ..\libs\math-library\src\*.hpp libs
copy ..\libs\obj-loader\src\*.h* libs
copy ..\libs\obj-loader\build\src\libobj-loader.a libs
copy ..\libs\triangulate\src\*.hpp libs
copy ..\libs\profiler\src\*.h* libs
copy ..\libs\profiler\build\src\libprofiler-library.a libs