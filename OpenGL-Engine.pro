#-------------------------------------------------
#
# Project created by QtCreator 2019-06-11T14:39:59
#
#-------------------------------------------------

QT       += core gui opengl widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OpenGL-Engine
TEMPLATE = lib
INCLUDEPATH += libs/math-library/src

CONFIG += staticlib
CONFIG += c++11

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp

SOURCES += src/glwidget.cpp \
        src/glwindow.cpp\
        src/getgpuinfo.cpp

HEADERS  += \
        src/glwidget.h\
        src/libs/OBJ.h\
        src/libs/Triangulate.hpp\
        src/libs/MathLibrary.hpp \
        src/libs/Profiler.h\
        src/model.hpp \
        src/glwindow.h

FORMS    += \
    src/glwindow.ui

RESOURCES += \
    glresources.qrc

DISTFILES += \
    shader.frag \
    shader.vert


# OBJ LOADER
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/ -lobj-loader
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/ -lobj-loader
else:unix: LIBS += -L$$PWD/src/libs/ -lobj-loader

INCLUDEPATH += $$PWD/src/libs
DEPENDPATH += $$PWD/src/libs

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libobj-loader.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libobj-loader.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/obj-loader.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/obj-loader.lib
else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libobj-loader.a


# PROFILER
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/ -lprofiler-library
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/ -lprofiler-library
else:unix: LIBS += -L$$PWD/src/libs/ -lprofiler-library

INCLUDEPATH += $$PWD/src/libs
DEPENDPATH += $$PWD/src/libs

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libprofiler-library.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libprofiler-library.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/profiler-library.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/profiler-library.lib
else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libprofiler-library.a
