rm -rf src/libs
git submodule update --init --recursive
git submodule update --recursive --remote

cd libs/obj-loader
./build.sh
cd ..

cd profiler
./build.sh
cd ../..

mkdir src/libs
cp libs/math-library/src/*.hpp src/libs/
cp libs/obj-loader/src/*.hpp src/libs/
cp libs/obj-loader/src/*.h src/libs/
cp libs/obj-loader/bin/libobj-loader.a src/libs/
cp libs/triangulate/src/*.hpp src/libs/
cp libs/profiler/bin/libprofiler-library.a src/libs/
cp libs/profiler/src/*.h* src/libs/
