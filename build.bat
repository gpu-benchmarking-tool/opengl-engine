rmdir /s /Q build
mkdir build
cd build
qmake ../OpenGL-Engine.pro -platform win32-g++
mingw32-make
cd ..

rmdir /s /Q bin
mkdir bin
copy src\*.h* bin
cd bin
mkdir libs
mkdir assets
copy ..\src\libs\* libs
cd ..
copy build\release\libOpenGL-Engine.a bin
copy assets\OBJ\*.obj bin\assets