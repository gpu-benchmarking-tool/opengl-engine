rm -rf build
mkdir build
cd build
qmake ../OpenGL-Engine.pro
make

cd ..
rm -rf bin
mkdir bin
cp src/*.h* bin
cp src/libs bin -r
cp build/libOpenGL-Engine.a bin
mkdir bin/assets
cp assets/OBJ/*.obj bin/assets